<?php

namespace App\System\Config;

use Symfony\Component\Config\FileLocator;

class Config implements IConfig 
{
    private $config = [];
    private $loader;
    private $locator;

    public function __construct($dir) {
        $directories = [
            BASEPATH.'/'.$dir
        ];

        $this->setLocator($directories);
        $this->setLoader();
    }
    
    public function addConfig($file) {
        $configValues = $this->loader->load($this->locator->locate($file));
        if($configValues) {
            foreach($configValues as $key => $arr) {
                $this->config[$key] = $arr;
            }
        }
    }

    /**
     * something like a service-container
     * get('database');
     * get('database.dbname');
     * in controller app()->get('config')->get('database.dbname');
     * @param $keyValue
     * @return mixed|null
     */
    public function get($keyValue) {
        dump($keyValue);
        // todo добаввить проверки для корректновти yaml файла

        list($key, $value) = explode('.', $keyValue);

        if($key && isset($this->config[$key])) {
            if($value && isset($this->config[$key][$value])) {
                return $this->config[$key][$value];
            }
            else {
                return $this->config[$key];
            }
        }

        return null;
    }

    // something like getter a service-container
    public function getConfig() {
        return $this->config;
    }
    
    public function setLoader() {
        $this->loader = new YamlConfigLoader($this->locator);
    }
    
    public function setLocator($dir) {
        $this->locator = new FileLocator($dir);
    }
}