<?php

namespace App\System\Database;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;

class Orm
{
    private $params;
    private $entityManager;

    public function __construct($params)
    {
        $this->params = $params;
        $config = Setup::createAnnotationMetadataConfiguration(["src/Entity", true]);
        try {
            $entityManager = EntityManager::create($this->params, $config);
            $this->setEntityManager($entityManager);
        } catch (ORMException $e) {
            echo $e->getMessage();
        }
    }

    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }
}
