<?php

namespace App\Http;

use App\Entities\Book;

class IndexController extends Controller
{

    public function indexAction(){
//        get date from service-container
//        dump(app()->get('config')->get('database.dbname'));

        $entityManager = app()->get('orm')->getEntityManager();
        dump($entityManager);

        $book = new Book();
        $book->title = 'Test title';
        $entityManager->persist($book);
        $entityManager->flush();
        dump($book);

        return $this->render('index', ['title'=>'Index page']);
    }
}
