<?php

namespace App\Http;


class PageController extends Controller
{

    public function indexAction($alias){
        return $this->render('page', ['alias'=>$alias]);
    }
}
