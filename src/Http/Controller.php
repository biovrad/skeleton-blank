<?php

namespace App\Http;

use App\System\View\View;
use App\System\Controller\IController;
use Symfony\Component\HttpFoundation\Response;


abstract class Controller implements IController
{
    protected $view;

    public function render($path, $data = [])
    {
        return new Response($this->view->make($path, $data));
    }

    public function __construct()
    {
        $this->view = new View();
    }
}
